import os
import gc
import sys
import time
import threading
import resource

import psutil

class Measure(threading.Thread):
    def __init__(self, f, args=(), kwargs={}):
        threading.Thread.__init__(self)
        self.f = f
        self.args = args
        self.kwargs = kwargs
        self.elapsed_time = 0
        self.ret = None

    def run(self):
        start = self.get_time()
        self.ret = self.f(*self.args, **self.kwargs)
        self.elapsed_time = self.get_time() - start

        if type(self.ret) is float:
            self.ret = None

    def get_time(self):
        return time.clock_gettime(time.CLOCK_THREAD_CPUTIME_ID)

class Profiler:
    def __init__(self):
        self.process = psutil.Process(os.getpid())

    def get_time(self, f, args=(), kwargs={}):
        """ Track time taken to execute function. """
        search_t = Measure(f, args, kwargs)
        search_t.start()
        search_t.join()

        return search_t.ret, search_t.elapsed_time

    def get_space(self, f, args=(), kwargs={}):
        """ Track memory peak when executing function. """
        kwargs['track_mem'] = True
        return f(*args, **kwargs)

    def get_memory_usage(self):
        """ Return the RSS (Resident Set Size) of the current process. The RSS is
        the memory used that resides in main memory, not including swap.
        """
        return self.process.memory_info().rss
