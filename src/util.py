import os
import gc
import sys
import math
import types
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.patches as patches

import robot

BLACKLIST = type, types.ModuleType, types.FunctionType

cmap = {
    'initial': (0.3, 1, 0, 0.9),
    'goal': (1, 0, 0, 1.0),
    'path': (1, 1, 0, 1.0),
    'wall': 0.8,
    'background': 0.15
}

def generate_problem(shape=(64,64), random=False, wall_p=0.3, state_pos=0.1):
    """Generate a map for the robot to navigate. In case
    random is False, the walls are chosen randomly with density wall_p and size
    given by shape. Otherwise, the default map specified in the instructions is
    used"""

    if random:
        map = np.random.random(shape)
        map = np.floor(map + (1-wall_p))

        # Determine the positions for initial and goal
        rows, cols = shape
        i_x, i_y = int(rows*state_pos), int(cols*state_pos) # Approximately bottom-left corner
        g_x, g_y = shape[0]-i_x, shape[1]-i_y               # Approximately upper-right corner

        map[i_y-1:i_y+1, i_x-1:i_x+1] = 1.0
        map[g_y-1:g_y+1, g_x-1:g_x+1] = 1.0

    else:
        map = np.ones((64, 64))
        map[:40, 20] = 0
        map[20:, 40] = 0
        i_x, i_y = 10, 10
        g_x, g_y = 50, 50

    # We create a float32 map to allow a larger range for colors
    map = map.astype(np.float32)

    return robot.RobotProblem(map, (i_x, i_y), (g_x, g_y))

def display(problem, path_node=None, save=False, name='tmp'):
    rows = problem.rows
    cols = problem.cols

    # We want at least 4 pixels for each point
    min_w = max(cols//25, 1)
    min_h = max(rows//25, 1)
    fig, ax = plt.subplots(figsize=(min_w + 4, min_h + 4))

    map = problem.map.copy()
    map[np.where(map == 1.0)] = cmap['background']
    map[np.where(map == 0.0)] = cmap['wall']

    norm = colors.Normalize(vmin=0, vmax=1)
    img = ax.imshow(map, cmap='inferno', norm=norm)

    # Get the pixel size of the axis
    bbox = ax.get_window_extent().transformed(fig.dpi_scale_trans.inverted())
    width, height = bbox.width, bbox.height
    width = width*fig.dpi - 20
    height = height*fig.dpi - 20

    # Define markersize according to axis width
    d = math.floor(min(width/rows, height/cols)) # Get the smallest dimension for marker
    markersize = (d/2)**2                        # Marker size is the area in pixels**2

    ax.scatter(problem.initial[0], problem.initial[1],
        color=cmap['initial'],
        marker='o',
        s=markersize*3,
        linestyle='None',
        label='initial_state'
    )
    ax.scatter(problem.goal[0], problem.goal[1],
        color=cmap['goal'],
        marker='o',
        s=markersize*3,
        linestyle='None',
        label='goal_state'
    )

    if type(path_node) is not float and path_node:
        node_list = path_node.path()
        xs = [node.state[0] for node in node_list]
        ys = [node.state[1] for node in node_list]

        ax.scatter(xs, ys,
            color=cmap['path'],
            marker='o',
            s=markersize*0.5,
            linestyle='None',
            label='solution'
        )

    ax.legend()
    ax.set_ylim(0, rows-1)

    if save and name is not None:
        fig.savefig(os.path.join(save, f'{name}.pdf'))
    else:
        fig.waitforbuttonpress()
        plt.close(fig)

    return fig

def plot_complexity(x, ys, search_list, out_dir, name):

    fig, ax = plt.subplots(figsize=(10, 10))
    for i, y in enumerate(ys):
        ax.plot(x, y, label=search_list[i]['abbrev'])

    ax.legend()
    if name == 'time':
        ax.set_ylabel('Tempo (s)')
    elif name == 'space':
        ax.set_ylabel('Memória (MB)')
    ax.set_xlabel('n')
    ax.set_ylim(0, 1.1*np.max(ys))
    ax.set_xlim(np.min(x), np.max(x))
    fig.savefig(os.path.join(out_dir, f'{name}.pdf'))

def get_sizeof(obj):
    """ Sum size of object & members."""
    if isinstance(obj, BLACKLIST):
        raise TypeError('getsize() does not take argument of type: '+ str(type(obj)))
    seen_ids = set()
    size = 0
    objects = [obj]
    while objects:
        need_referents = []
        for obj in objects:
            if not isinstance(obj, BLACKLIST) and id(obj) not in seen_ids:
                seen_ids.add(id(obj))
                size += sys.getsizeof(obj)
                need_referents.append(obj)
        objects = gc.get_referents(*need_referents)
    return size
