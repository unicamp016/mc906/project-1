\section{Modelagem do Problema}\label{sec:model}

\subsection{Definição do Ambiente}
  Dado o problema proposto na Seção~\ref{sec:proposal}, sabemos que o ambiente é caracterizado como:

  \begin{itemize}
    \item \textbf{parcialmente observável} - o robô só sabe o seu estado atual (\emph{i.e.} posição no mapa) e pode mover-se apenas para posições adjacentes;
    \item \textbf{determinístico} - dado um estado e uma ação, o resultado sempre será o mesmo;
    \item \textbf{conhecido} - sabemos para que posição cada ação nos leva;
    \item \textbf{agente único} - há um único robô no mapa.
  \end{itemize}

  Poderíamos tratar o ambiente como sendo \textbf{episódico} ou \textbf{sequencial}. Entretanto, veremos mais adiante que tratar o ambiente como episódico leva-nos a soluções muito demoradas e para ter um desempenho satisfatório é importante guardar as posições já visitadas ou o custo do caminho percorrido até então. Para facilitar a modelagem, também podemos assumir que o ambiente é \textbf{discreto}, ou seja, o robô possui um conjunto finito de ações a partir de qualquer estado. Finalmente, podemos assumir que o ambiente é \textbf{estático}, tornando possível a aplicação de um algoritmo de busca para encontrar uma solução na forma de uma sequência de ações antes de iniciar a fase de execução.

\subsection{Definição do Problema}\label{sec:problem-definition}
  Um estado é representada por uma pose $(x, y)$ no mapa. O estado inicial do robô é uma pose arbitrária no mapa onde o robô pode se locomover livremente. Na Figura~\ref{fig:map}, a pose inicial é $(10, 10)$.

  Para simplificar a discretização das ações assumimos que o robô é omnidirecional, tornando ângulo $\theta$ irrelevante para a caracterização de uma ação (podemos assumir que $\theta$ sempre vale 0). Dessa forma, o conjunto de ações disponíveis para o agente em um estado são todas as posições transponíveis na sua vizinhança-8. A vizinhança-8 de uma pose $(x_i, y_i)$ é dada por todas as poses $(x_v, y_v)$ que pertencem ao conjunto dado pela Equação~\ref{eq:8-neighborhood}.

  \begin{equation}
    V_8 = \{(x_v, y_v) \in \mathbb{Z} \times \mathbb{Z} : |x_i - x_v| \leq 1 ~~\text{e}~~|y_i - y_v| \leq 1\} \label{eq:8-neighborhood}
  \end{equation}

  Como optou-se por representar o conjunto de ações disponíveis pelas posições transponíveis na vizinhança-8 do robô, o modelo de transição torna-se extremamente simples: dado um estado e uma ação, o estado resultante é a posição da própria ação.

  O teste de objetivo pode ser feito comparando a pose atual do robô com a pose objetivo. Se ambas forem iguais, a busca termina.

  Apesar do robô ser omnidirecional, não seria razoável atribuir o mesmo custo para todas as ações, já que mover-se na diagonal, por exemplo, percorre uma distância euclidiana maior no mapa do que mover-se lateralmente. Portanto, adotou-se um custo de $\sqrt{2}$ aos movimentos diagonais e 1 para os demais.

  \subsection{Heurísticas}\label{sec:heuristics}
  Para garantir a otimicidade do $A^*$, devemos escolher heurísticas \textbf{consistentes}, já que o algoritmo será usado para realizar a busca em grafos. Uma heurística $h(n)$ é dita consistente se para todo nó $n$ e todo sucessor $n'$ de $n$ gerado a partir de uma ação $a$, a inequação dada pela Equação~\ref{eq:consistency} é satisfeita~\cite{russell2010aima}.

  \begin{equation}\label{eq:consistency}
    h(n) \leq c(n, a, n') + h(n')
  \end{equation}

  Intuitivamente, devemos escolher heurísticas que nunca sobreestimam o custo real de chegar de um nó ao objetivo. Ao buscar heurísticas para o nosso problema, considerou-se a distância Manhattan, dada pela Equação~\ref{eq:manhattan}, e a distância euclidiana, dada pela Equação~\ref{eq:euclidean}, como candidatas. Entretanto, optamos apenas pela euclidiana, já que a Manhattan quebrava a propriedade de consistência do problema definido na Seção~\ref{sec:problem-definition}.

  \begin{equation}\label{eq:manhattan}
    d_m = |x_1 - x_2| + |y_1 - y_2|
  \end{equation}

  \begin{equation}\label{eq:euclidean}
    d_e = \sqrt{(x_1 - x_2)^2 + (y_1 + y_2)^2}
  \end{equation}

  Considere como exemplo a tarefa de ir do ponto inicial $(x_i, y_i) = (0, 0)$ ao objetivo $(x_g, y_g) = (10, 10)$. Nesse caso, se nos movimentarmos na diagonal para o ponto $(1,1)$, usando a distância Manhattan, teríamos que $h(n_i) = |10-0| + |10-0| = 20$, $c(n, a, n') = \sqrt{2}$ e $h(n') = |10-1| + |10-1| = 18$, quebrando a propriedade de consistência da Equação~\ref{eq:consistency}. Uma forma de remediar isso, é associando um custo de 2 para os movimentos diagonais, tornando consistentes ambas as medidas de distância. Porém, atribuir um custo de 2 resulta em um comportamento indesejável para nossa busca, já que ao usar a distância euclidiana como heurística, acabaríamos favorecendo a movimentação lateral ao invés dos movimentos diagonais e ao usar a distânica Manhattan, haveria um empate entre as duas opções. Dessa forma, foram escolhidas as seguintes heurísticas que são consistentes dado o problema definido na Seção~\ref{sec:problem-definition}:

  \subsubsection{Distância Euclidiana}
    A distância euclidiana definida pela Equação~\ref{eq:euclidean} entre a pose atual e a pose objetivo.

  \subsubsection{Distância Horizontal}
    O módulo da diferença nas coordenadas $x$ entre a pose atual e a pose objetivo.

  \subsubsection{Distância Vertical}
    O módulo da diferença nas coordenadas $y$ entre a pose atual e a pose objetivo.

  A consistência das três heurísticas vem do fato que o valor de $c(n, a, n')$ para quaisquer $n$ e $n'$ adjacentes sempre será maior ou igual do que a diferença $\epsilon = h(n)-h(n')$. Para a distânica euclideana, movimentos diagonais nos dão no máximo $\epsilon = \sqrt{2}$ e sempre temos que $c = \sqrt{2}$. Além disso, pela desigualdade triangular, temos que movimentos laterais nos dão um $\epsilon \leq 1$, enquanto $c$ sempre vale 1. Como a distância euclideana é sempre maior ou igual que a distância vertical ou horizontal, temos que essas também são consistentes.
