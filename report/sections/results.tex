\section{Resultados e Discussão}\label{sec:results}
  Nesta seção serão apresentados os resultados para os experimentos de visualização e desempenho dos algoritmos de busca informada e não-informada.

  \subsection{Completude e Otimicidade}

  Dado que foram escolhidos apenas algoritmos de busca em grafos e que o número de estados do problema é finito para $n$ finito, temos que todos os algoritmos escolhidos são completos, isto é, eles sempre encontrarão uma solução.

  Com relação a otimicidade, os únicos algoritmos ótimos são a busca de custo uniforme e o $A^*$. A otimicidade da busca de custo uniforme vem da natureza do algoritmo, que expande os nós em ordem do custo do custo do caminho ótimo. O $A^*$ é ótimo devido à escolha de heurísticas consistentes na Seção~\ref{sec:heuristics}. A busca gulosa, bidirecional, o BFS e o DFS não são ótimos pois nem consideram o custo das ações ou do caminho ao buscar por uma solução.

  \subsection{Visualização}\label{sec:visualization-discussion}

  O resultado das visualizações permitem-nos entender como é feita a tomada de decisão nos algoritmos de busca. A visualização dos algoritmos de busca não-informada no mapa da Figura~\ref{fig:map} é dada pela Figura~\ref{fig:uninformed-searches}. Na Figura~\ref{fig:bfs}, pode-se observar que o caminho encontrado pelo BFS não é ótimo, já que são feitos inúmeros movimentos diagonais desnecessariamente. Também é possível observar como o mapa torna-se mais claro a medida que nos distanciamos do ponto inicial, sinal que a busca explorou todos os nós mais próximos antes dos mais distantes. A Figura~\ref{fig:dfs} mostra o comportamento do DFS, que explora quase o mapa todo, encontrando um caminho com 1133 nós até o objetivo. Essa busca também não é ótima e a ordem de exploração dos nós e definida pela ordem em que as ações são retornadas na implementação. O resultado da busca de custo uniforme está indicado na Figura~\ref{fig:uniform-cost}. Ao contrário do BFS, a de custo uniforme é ótima, visto que nosso mapa é finito e os custos das ações são sempre positivos. Isso está refletido na decisão de sempre priorizar movimentos horizontais e verticais sobre os movimentos diagonais, que têm um custo mais alto. Na solução da Figura~\ref{fig:uniform-cost}, após passar a segunda parede, o robô escolhe movimentar-se para cima o máximo possível, enquanto na solução da Figura~\ref{fig:bfs} o robô opta por caminhar na diagonal. Como a busca bidirecional não segue a mesma API das demais, não recuperamos a solução encontrada pelo algoritmo. Entretanto na Figura~\ref{fig:bidirectional}, podemos ver pelas cores mais escuras em torno dos pontos inicial e objetivo, que a exploração é feita de forma alteranda, ora expandindo o nó objetivo e ora o nó inicial. Pela coração do mapa, fica visível que entre as buscas não-informadas, a bidirecional é a que menos explora desnecessariamente o mapa, encontrando uma solução explorando $O(b^{\frac{d}{2}})$ nós, enquanto o BFS e a de custo uniforme demora na ordem de $O(b^d)$.

  \begin{figure}[htbp]
  	\centering
    \quad
  	\subfloat[BFS]{
  		\label{fig:bfs}
  		\includegraphics[width=.4\textwidth]{./images/breadth_first_search.pdf}
  	}
    \quad
  	\subfloat[DFS]{
  		\label{fig:dfs}
  		\includegraphics[width=.4\textwidth]{./images/depth_first_search.pdf}
  	} \\
    \quad
    \subfloat[Custo uniforme]{
  		\label{fig:uniform-cost}
  		\includegraphics[width=.4\textwidth]{./images/uniform_cost_search.pdf}
  	}
    \quad
  	\subfloat[Bidirecional]{
  		\label{fig:bidirectional}
  		\includegraphics[width=.4\textwidth]{./images/bidirectional_search.pdf}
  	} \\
  	\caption{Solução encontrada pelos algoritmos de busca não-informada para o mapa da Figura~\ref{fig:map}. Estão inidicadas em amarelo as soluções encontradas pelo BFS~\protect\subref{fig:bfs}, DFS~\protect\subref{fig:dfs} e a busca de custo uniforme~\protect\subref{fig:uniform-cost}. A busca bidirecional~\protect\subref{fig:bidirectional} apresenta apenas as cores indicativas dos nós explorados.}\label{fig:uninformed-searches}
  \end{figure}

  O resultado dos algoritmos de busca informada aplicados no mapa da Figura~\ref{fig:map} é dado pela Figura~\ref{fig:informed-searches}. Note que em geral, todos os mapas das buscas informadas são menos exploradas que os das buscas não-informadas. O resultado do $A^*$ com heurística de distância euclideana pode ser visto na Figura~\ref{fig:astar-euclidean}. Note como existe uma mancha escura na direção da posição objetivo, indicando que a exploração começou nesse sentido. As heurísticas de distância vertical e horizontal, indicadas nas Figuras~\ref{fig:astar-vertical} e~\ref{fig:astar-horizontal}, respectivamente, têm um comportamento similar, com as manchas escuras indo na direção indicada pela heurística. Entre as três figuras, é possível observar que o $A^*$ euclideano tem o mapa com o menor número de nós explorados, sinal que essa heurística tem o menor fator efetivo de ramificação. Da definição das duas heurísticas, temos que a distância euclidiana domina as outras duas, o que traduz diretamente para uma maior eficiência. Como só são expandidos os nós cujo custo total é menor que o custo ótimo até o nó objetivo (\emph{i.e.} $f(n) \leq C^*$), o $A^*$ com heurística de distância euclidiana vai necessariamente expandir menos nós. Apesar disso, todas heurísticas encontram uma solução ótima e fazem-no explorando menos nós que a busca de custo uniforme na Figura~\ref{fig:uniform-cost}. A busca gulosa com heurística de distância euclidiana está indicada na Figura~\ref{fig:greedy}. No mapa da Figura~\ref{fig:map}, essa busca tem o melhor desempenho, explorando o menor número de nós entre as demais. Entretanto, a solução encontrada não é ótima.

  \begin{figure}[htbp]
  	\centering
    \quad
  	\subfloat[$A^*$ euclidiano]{
  		\label{fig:astar-euclidean}
  		\includegraphics[width=.4\textwidth]{./images/astar_sld_search.pdf}
  	}
    \quad
  	\subfloat[$A^*$ vertical]{
  		\label{fig:astar-vertical}
  		\includegraphics[width=.4\textwidth]{./images/astar_vertical_dist.pdf}
  	} \\
    \quad
    \subfloat[$A^*$ horizontal]{
  		\label{fig:astar-horizontal}
  		\includegraphics[width=.4\textwidth]{./images/astar_horizontal_dist.pdf}
  	}
    \quad
  	\subfloat[Gulosa]{
  		\label{fig:greedy}
  		\includegraphics[width=.4\textwidth]{./images/greedy_best_first_search.pdf}
  	} \\
  	\caption{Solução encontrada pelos algoritmos de busca informada para o mapa da Figura~\ref{fig:map}. Estão inidicadas em amarelo as soluções encontradas pelo $A^*$ com heurística de distância euclidiana~\protect\subref{fig:astar-euclidean}, distânica vertical~\protect\subref{fig:astar-vertical}, distância horizontal~\protect\subref{fig:astar-horizontal} e busca gulosa com heurística de distância euclideana~\protect\subref{fig:greedy}}.\label{fig:informed-searches}
  \end{figure}

  As Figuras~\ref{fig:random-uninformed-searches} e~\ref{fig:random-informed-searches} mostram a aplicação dos algoritmos de busca não-informada e informada, respectivamente, no mapa aleatório da Figura~\ref{fig:random-map}. A análise feita para o mapa da Figura~\ref{fig:map} também vale para esse e o comportamento dos algoritmos é similar.

  \begin{figure}[tbp]
  	\centering
    \quad
  	\subfloat[BFS]{
  		\label{fig:random-bfs}
  		\includegraphics[width=.4\textwidth]{./images/random_breadth_first_search.pdf}
  	}
    \quad
  	\subfloat[DFS]{
  		\label{fig:random-dfs}
  		\includegraphics[width=.4\textwidth]{./images/random_depth_first_search.pdf}
  	} \\
    \quad
    \subfloat[Custo uniforme]{
  		\label{fig:random-uniform-cost}
  		\includegraphics[width=.4\textwidth]{./images/random_uniform_cost_search.pdf}
  	}
    \quad
  	\subfloat[Bidirecional]{
  		\label{fig:random-bidirectional}
  		\includegraphics[width=.4\textwidth]{./images/random_bidirectional_search.pdf}
  	} \\
  	\caption{Solução encontrada pelos algoritmos de busca não-informada para o mapa da Figura~\ref{fig:random-map}. Estão inidicadas em amarelo as soluções encontradas pelo BFS~\protect\subref{fig:random-bfs}, DFS~\protect\subref{fig:random-dfs} e a busca de custo uniforme~\protect\subref{fig:random-uniform-cost}. A busca bidirecional~\protect\subref{fig:random-bidirectional} apresenta apenas as cores indicativas dos nós explorados.}\label{fig:random-uninformed-searches}
  \end{figure}

  \begin{figure}[tbp]
  	\centering
    \quad
  	\subfloat[$A^*$ euclidiano]{
  		\label{fig:random-astar-euclidean}
  		\includegraphics[width=.4\textwidth]{./images/random_astar_sld_search.pdf}
  	}
    \quad
  	\subfloat[$A^*$ vertical]{
  		\label{fig:random-astar-vertical}
  		\includegraphics[width=.4\textwidth]{./images/random_astar_vertical_dist.pdf}
  	} \\
    \quad
    \subfloat[$A^*$ horizontal]{
  		\label{fig:random-astar-horizontal}
  		\includegraphics[width=.4\textwidth]{./images/random_astar_horizontal_dist.pdf}
  	}
    \quad
  	\subfloat[Gulosa]{
  		\label{fig:random-greedy}
  		\includegraphics[width=.4\textwidth]{./images/random_greedy_best_first_search.pdf}
  	} \\
  	\caption{Solução encontrada pelos algoritmos de busca informada para o mapa da Figura~\ref{fig:random-map}. Estão inidicadas em amarelo as soluções encontradas pelo $A^*$ com heurística de distância euclidiana~\protect\subref{fig:random-astar-euclidean}, distânica vertical~\protect\subref{fig:random-astar-vertical}, distância horizontal~\protect\subref{fig:random-astar-horizontal} e busca gulosa com heurística de distância euclideana~\protect\subref{fig:random-greedy}}.\label{fig:random-informed-searches}
  \end{figure}


  \subsection{Desempenho}

  As Figuras~\ref{fig:time} e~\ref{fig:space} mostram o tempo e consumo de memória médios, respecitavemente, para cada algoritmo com diferentes níveis de discretização do mapa. A primeira observação que pode ser feita na Figura~\ref{fig:time} é que o tempo de execução do DFS é muito maior que dos demais algoritmos, apesar dele explorar aproximadamente o mesmo número de nós que o BFS e a busca de custo uniforme. Isso é resultado da implementação do algoritmo na biblioteca em \texttt{Python} do AIMA~\cite{russell2010aima}, onde o desenvolvedor optou por usar uma lista do \texttt{Python} como uma pilha que guarda os nós na fronteira (\emph{i.e.} os próximos nós a serem explorados em ordem FIFO). O problema é que, por natureza, o DFS acumula vários nós na fronteira conforme vai se aprofundando na busca e isso torna muito custosa a verificação se um nó já pertence à fronteira ou não. Essa verificação é feita em tempo $O(n)$ no tamanho da lista, enquanto que a verificação para saber se um nó já foi explorado é feita em tempo amortizado $O(1)$, já que os nós explorados são armazenado em um conjunto hash. Dessa forma, o BFS, que tem a maioria dos nós no conjunto de nós explorados, é muito mais rápido que o DFS, apesar dos dois explorarem aproximadamente o mesmo número de nós. Os outros algoritmos de busca não sofrem desse problema, já que durante a execução, pouquíssimos nós permanecem na fronteira e a maioria encontra-se no conjunto de nós explorados.

  Para a maioria dos algoritmos, o tempo de execução é maior para as buscas que exploram um número maior de nós. Entretanto, na Figura~\ref{fig:time}, vemos que isso não vale para o BFS, que explora quase o mapa inteiro mas tem um tempo de execução inferior aos algoritmos de busca informada. Isso é devido, principalmente ao tempo gasto computando as heurísticas, que para instâncias pequenas do problema acaba sendo mais custoso do que explorar mais nós. Além disso, a busca bidirecional tem um custo surpreendentemente alto, visto que o número de nós explorados é significativamente menor que o da busca de custo uniforme e o $A^*$ vertical e horizontal. Isso é devido principalmente à sua implementação, que é bem mais investida do que a dos demais algoritmos. Apesar de assintoticamente essa busca ser melhor, para mapas de dimensões até $256 \times 256$, essa discrepância ainda é pouco acentuada.

  No gráfico da Figura~\ref{fig:space} o custo de memória do DFS também salta aos olhos por destoar muito dos demais. Entretanto, isso é novamente resultado da implementação que usa uma lista do \texttt{Python} para armazenar os nós da fronteira. Como a lista consome muito mais memória do que um conjunto hash, o DFS acaba sendo o algoritmo mais custoso. Para os outros algoritmos, o consumo de memória está fortemente relacionado ao número de nós explorados. Pode-se ver que a busca gulosa com heurística de distância euclidiana é a mais barata, seguida pelo $A^*$ euclidiano e a busca bidirecional, como era de se esperar a partir da visualização nas Figuras~\ref{fig:uninformed-searches} e~\ref{fig:informed-searches}. Os demais algoritmos têm um custo de memória muito semelhante pois exploram aproximadamente o mesmo número de nós.

  \begin{figure}
    \centering
    \includegraphics[width=0.83\textwidth]{./images/time.pdf}
    \caption{Tempo médio entre {\iterations} execuções com mapas aleatórios de todos os algoritmos de busca para diferentes níveis de discretização do mapa.}\label{fig:time}
  \end{figure}

  \begin{figure}
    \centering
    \includegraphics[width=0.83\textwidth]{./images/space.pdf}
    \caption{Consumo de memória médio entre {\iterations} execuções com mapas aleatórios de todos os algoritmos de busca para diferentes níveis de discretização do mapa.}\label{fig:space}
  \end{figure}
