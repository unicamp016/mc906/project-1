import gc
import os
import argparse

import robot
import util
import profiler
import aima.search as ams

import numpy as np
import matplotlib.pyplot as plt

search_list = \
    [
        {
            'fn': ams.bidirectional_search,
            'abbrev': 'Bidirecional',
            'filename': 'bidirectional_search'
        },
        {
            'fn': ams.depth_first_graph_search,
            'abbrev': 'DFS',
            'filename': 'depth_first_search'
        },
        {
            'fn': ams.uniform_cost_search,
            'abbrev': 'Custo Uniforme',
            'filename': 'uniform_cost_search'
        },
        {
            'fn': ams.breadth_first_graph_search,
            'abbrev': 'BFS',
            'filename': 'breadth_first_search'
        },
        {
            'fn': ams.astar_search,
            'abbrev': 'A* Euclidiano',
            'filename': 'astar_sld_search'
        },
        {
            'fn': ams.astar_search,
            'abbrev': 'A* Horizontal',
            'filename': 'astar_horizontal_dist',
            'kwargs': {
                'h': 'h_2'
            }
        },
        {
            'fn': ams.astar_search,
            'abbrev': 'A* Vertical',
            'filename': 'astar_vertical_dist',
            'kwargs': {
                'h': 'h_3'
            }
        },
        {
            'fn': ams.greedy_best_first_graph_search,
            'abbrev': 'Guloso',
            'filename': 'greedy_best_first_search'
        }
    ]

params = {
    'n': [i for i in range(16, 257, 16)],
    'samples': 100,
    'time_log': 'times.txt',
    'mem_log': 'mems.txt'
}

def plot_log(filename, output_dir, name):
    with open(filename, 'r') as f:
        lines = f.readlines()

    ns = []
    metric = [[] for _ in range(len(search_list))]
    for l in lines:
        for i, token in enumerate(l.strip().split(' ')):
            token = float(token)
            if not i:
                ns.append(token)
            else:
                if name == 'space':
                    token /= 1000000
                metric[i-1].append(token)

    for m in metric:
        print(m)

    util.plot_complexity(
        x=ns,
        ys=metric,
        search_list=search_list,
        out_dir=output_dir,
        name=name
    )

def main(output_dir, log_dir):

    prof = profiler.Profiler()

    # Save the original search map before any search algorithm
    robot_problem = util.generate_problem(random=False)
    util.display(robot_problem, save=output_dir, name='map')

    for search in search_list:
        # Reset map since we modified it to plot colors
        robot_problem.reset_map()

        # Create a dictionary with keyword arguments for search
        fn = search['fn']
        kwargs = search.get('kwargs', {})
        kwargs['problem'] = robot_problem

        # Run search algorithm
        path_node, elapsed_time = prof.get_time(fn, kwargs=kwargs)

        # Save explored map to disk
        util.display(robot_problem, path_node=path_node, save=output_dir,
            name=f"{search['filename']}")

        print(
            f"{search['abbrev']}:\n"\
            f"\tTime: {elapsed_time:.3f}s"
        )

    time_f = open(os.path.join(log_dir, params['time_log']), 'w')
    mem_f = open(os.path.join(log_dir, params['mem_log']), 'w')

    times = [[] for _ in range(len(search_list))]
    mems = [[] for _ in range(len(search_list))]
    for n in params['n']:
        print(f"Running tests for n={n}...")
        for j in range(params['samples']):
            robot_problem = util.generate_problem(shape=(n,n), random=True, wall_p=0.3)

            n_times = [[] for _ in range(len(search_list))]
            n_mems = [[] for _ in range(len(search_list))]

            for i, search in enumerate(search_list):
                robot_problem.reset_map()
                kwargs = search.get('kwargs', {})
                kwargs['problem'] = robot_problem
                _, time = prof.get_time(
                    f=search['fn'],
                    kwargs=kwargs
                )

                robot_problem.reset_map()
                kwargs = search.get('kwargs', {})
                kwargs['problem'] = robot_problem
                _, mem = prof.get_space(
                    f=search['fn'],
                    kwargs=kwargs
                )

                n_times[i].append(time)
                n_mems[i].append(mem)

        time_f.write(f'{n} ')
        for i, n_t in enumerate(n_times):
            mean_t = np.mean(n_t)
            time_f.write(f'{mean_t} ')
            times[i].append(mean_t)
        time_f.write('\n')
        time_f.flush()

        mem_f.write(f'{n} ')
        for i, n_m in enumerate(n_mems):
            mean_m = np.mean(n_m)
            mem_f.write(f'{mean_m} ')
            mems[i].append(mean_m)
        mem_f.write('\n')
        mem_f.flush()

    time_f.close()
    mem_f.close()

    util.plot_complexity(
        x=list(params['n']),
        ys=times,
        search_list=search_list,
        out_dir=output_dir,
        name='time'
    )

    util.plot_complexity(
        x=list(params['n']),
        ys=mems,
        search_list=search_list,
        out_dir=output_dir,
        name='space'
    )

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-o", "--output_dir", required=True)
    parser.add_argument("-l", "--log_dir", required=False, default='logs')
    args = parser.parse_args()
    main(args.output_dir, args.log_dir)
