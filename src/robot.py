# Std libs
import math

# Third-party libs
import numpy as np

# Custom libs
import aima.search

# One possible way to model the problem is to have the initial state and goal
# be (x, y) coordinates in our map and we have our board as an input


class RobotProblem(aima.search.Problem):
    """The problem of finding the path to take the robot from its initial
       state to his goal by searching the map."""

    def __init__(self, map, initial, goal, initial_color=0.2):
        """Defining a problem consists of generating the initial and goal
        states. In case random is False, we choose random positions where the
        robot is free to move and return them as the initial and goal states.
        Otherwise, we return the two fixed coordinates from the map in the
        instructions."""

        aima.search.Problem.__init__(self, initial, goal)
        self.map = map
        self.rows = map.shape[0]
        self.cols = map.shape[1]

        # Define attributes used for smooth colormap generation
        self.initial_color = initial_color
        self.current_color = self.initial_color
        self.mod = max((self.rows >> 6)*8, 1)
        self.count = 0

    def actions(self, position):
        """The actions at a graph node are all of the valid map positions that
        can be reached from the current position. Given that the robot is at
        position (x, y) and there are no boundary restrictions, this function
        can return up to 8 actions for the following positions:
                        [(x-1, y-1), (x, y-1), (x+1, y-1)]
                        [(x-1, y),    (x, y),    (x+1, y)]
                        [(x-1, y+1), (x, y+1), (x+1, y+1)]
        """
        moves = []
        x, y = position
        if x + 1 < self.cols:
            if self.map[y, x+1]:
                moves.append((x+1, y))      # Move right
            if y + 1 < self.rows and self.map[y+1, x+1]:
                moves.append((x+1, y+1))    # Move bottom-right
            if y - 1 >= 0 and self.map[y-1, x+1]:
                moves.append((x+1, y-1))    # Move upper-right
        if x - 1 >= 0:
            if self.map[y, x-1]:
                moves.append((x-1, y))      # Move left
            if y + 1 < self.rows and self.map[y+1, x-1]:
                moves.append((x-1, y+1))    # Move bottom-left
            if y - 1 >= 0 and self.map[y-1, x-1]:
                moves.append((x-1, y-1))    # Move upper-left
        if y + 1 < self.rows:
            if self.map[y+1, x]:
                moves.append((x, y+1))
        if y - 1 >= 0:
            if self.map[y-1, x]:
                moves.append((x, y-1))

        return moves

    def result(self, state, action):
        """The result of taking an action is the action itself, since the
        actions are represented as map positions."""
        return action

    def path_cost(self, c, state1, action, state2):
        x_diff = abs(state2[0] - state1[0])
        y_diff = abs(state2[1] - state1[1])
        if x_diff + y_diff > 1:
            return c + 1.41421
        else:
            return c + 1

    def explore(self, state):
        x, y = state
        if x < self.rows and y < self.cols:
            if self.map[y, x] > 0:
                self.count += 1
                if not self.count%self.mod:
                    self.current_color += 0.001
                self.map[y, x] = self.current_color

    def reset_map(self):
        # Reset all explored positions in the map
        self.current_color = self.initial_color
        self.map[np.where(self.map > 0)] = 1.0

    def find_min_edge(self):
        """Find minimum value of edges."""
        return 1

    def h(self, node):
        """h function is straight-line distance from a node's state to
          goal."""
        if type(node) == tuple:
            n_x, n_y  = node
        else:
            n_x, n_y = node.state
        g_x, g_y = self.goal
        return math.sqrt((n_x-g_x)**2 + (n_y-g_y)**2)

    def h_2(self, node):
        """h function is horizontal distance from the node's state to goal."""
        if type(node) == tuple:
            n_x, n_y  = node
        else:
            n_x, n_y = node.state
        g_x, g_y = self.goal

        return abs(g_x - n_x)

    def h_3(self, node):
        """h function is vertical distance from the node's state to goal."""
        if type(node) == tuple:
            n_x, n_y  = node
        else:
            n_x, n_y = node.state
        g_x, g_y = self.goal

        return abs(g_y - n_y)
